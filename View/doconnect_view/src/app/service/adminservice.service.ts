import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AdminserviceService {
  constructor() {}
  url = 'http://localhost:8080/';

  getAdminByEmail(email: string): Promise<any> {
    return fetch(this.url + `adminemail/${email}`, {
      method: 'GET',
    }).then((res) => res.json());
  }

  adminlogin(data: any): Promise<any> {
    return fetch(this.url + 'adminlogin', {
      method: 'POST',
      body: JSON.stringify(data),
      headers: {
        'Content-type': 'application/json',
      },
    }).then((res) => res.text());
  }

  adminregister(data: any): Promise<any> {
    return fetch(this.url + 'adminregister', {
      method: 'POST',
      body: JSON.stringify(data),
      headers: {
        'Content-type': 'application/json',
      },
    }).then((res) => res.text());
  }
}
